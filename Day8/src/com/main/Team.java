package com.main;
/***
 * @author gunda sneha
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Team {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberOfWords = 0;
		System.out.println("Enter the number of words:");
		numberOfWords = scanner.nextInt();
		System.out.println("Enter the number of strings to be searched:");
		String[] searchWords = new String[numberOfWords];
		for (int i = 0; i < numberOfWords; i++) {
			searchWords[i] = scanner.next();

		}
		File file = new File("C:\\\\Users\\\\gunda sneha\\\\Desktop\\\\Team.txt");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String text;
			int count = 0;
			try {
				while ((text = br.readLine()) != null) {
					for (String SearchString : searchWords) {
						count = countMatches(text.toLowerCase(), SearchString);
						System.out.println(SearchString + "count:" + count);
					}

				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static int countMatches(String text, String str) {
		if (text.isEmpty() || str.isEmpty()) {
			return 0;

		}
		Matcher matcher = Pattern.compile(str).matcher(text);
		int count = 0;
		while (matcher.find()) {
			count++;
		}
		return count;
	}

}
