package com.main;

import java.util.Scanner;

import com.pojo.UserMainCode;

public class SumofSquareMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number:");
		int number = scanner.nextInt();
		UserMainCode userMainCode = new UserMainCode();
		System.out.println(UserMainCode.sumOfSquareEvenDigits(number));
		scanner.close();
	}

}
