package com.main;
/*Java program that takes three numbers as input to calculate and print the average of the numbers
 * 
 */

import java.util.Scanner;

public class Average3 {

	public static void main(String[] args) {
		int num1, num2, num3;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter 3 numbers");
		num1 = scanner.nextInt();
		num2 = scanner.nextInt();
		num3 = scanner.nextInt();

		System.out.println(" Average of three numbers is: " + (num1 + num2 + num3) / 3);
		scanner.close();
		scanner = null;

	}

}
