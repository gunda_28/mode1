package com.main;

import java.util.Scanner;

import com.pojo.LargestWord;

public class LargestWordMain {

	public static void main(String[] args) {
		LargestWord largestWord = new LargestWord();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the sentence:");
		String sentence = scanner.nextLine();
		System.out.println(largestWord.getLargestWord(sentence));
		scanner.close();

	}

}
