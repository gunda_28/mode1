package com.pojo;
/*Java program to swap two variables
 * 
 */

public class Swap {
	public int swapNumbers(int num1,int num2) {
		System.out.println("Before swapping the numbers are:" +num1+ " " +num2);
		int temp;
		temp=num1;
		num1=num2;
		num2=temp;
		System.out.println("After swapping the numbers are:" +num1+ " " +num2);
		
		return 0;
		
	}

}
