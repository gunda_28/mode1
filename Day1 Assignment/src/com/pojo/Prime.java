package com.pojo;
/* The java program, which will take a number variable and check whether the number is prime or not. 
 * 
 */

public class Prime {
	public String checkPrimeNumber(int num) {
		int count=0;
		for(int i=1;i<=num;i++) {
			if(num%i==0) {
				count++;
		}
	}
	if(count==2)
			{
				return num + " is a prime number";
			}else {
				return num + " is not a prime number";
			}
	}

}
