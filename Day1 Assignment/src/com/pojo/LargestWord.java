package com.pojo;

import java.util.StringTokenizer;

public class LargestWord {

	public String getLargestWord(String sentence) {
		int max = 0;
		StringTokenizer st = new StringTokenizer(sentence, " ");
		while (st.hasMoreTokens()) {
			String string = st.nextToken();
			int num1 = string.length();
			if (num1 > max) {
				max = num1;
				sentence = string;
			}

		}
		return sentence;
	}
}

