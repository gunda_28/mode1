package com.main;

import java.util.Scanner;

public class SearchElementInArray {

	public static void main(String[] args) {
		int n;
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter the number of elements into array");
		n=scanner.nextInt();
		
		int arr[]=new int[n];
		System.out.println("Enter all elements");
		for (int i = 0; i < arr.length; i++) {
			arr[i]=scanner.nextInt();
		}
		int count=0;
		int key;
		System.out.println("Enter key element to search");
		key=scanner.nextInt();
		
		for (int i = 0; i < arr.length; i++) {
			
		
			if(key==arr[i]) {
				count=1;
				break;
			}
		}
		
		if(count==1) {
			System.out.println("key found");
		}
		else {
			System.out.println("key not found");
		}
scanner.close();
	}

}
