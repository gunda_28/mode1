package com.pojo;
/***
 * 
 * @author gunda sneha
 *
 */
/*
 * write a program to convert all the characters in a string to lowercase.
 */

public class StringLowerCase {
	public String toLowerCase(String input) // method for convert string
	{

		String output = input.toLowerCase();

		return output;
	}

}
