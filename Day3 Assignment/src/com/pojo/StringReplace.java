package com.pojo;

/***
 * 
 * @author gunda sneha
 *
 */
/*
 * program to read a string and return a modified string based on the following
 * rules. Return the String without the first 2 characters except when a. Keep
 * the first char if it is 'k' b. Keep the second char if it is 'b'.
 * 
 */
public class StringReplace {

	public String replace(String input) {

		String output = input.replace('d', 'k');
		return output;

	}
}
