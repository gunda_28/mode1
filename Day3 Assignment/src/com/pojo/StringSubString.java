package com.pojo;

/***
 * 
 * @author gunda sneha
 *
 */
/*
 * Accept a string, and two indices(integers), and print the substring
 * consisting of all characters inclusive range from ..to . Sample Input
 * Helloworld 3 7 Sample Output Lowo
 * 
 */

public class StringSubString {
	public String toSubstring(String input, int from, int to) {
		String output = " ";
		output = input.substring(from, to);// substring method
		return output;
	}

}
