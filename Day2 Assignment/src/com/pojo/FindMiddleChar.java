package com.pojo;

/***
 * 
 * @author gunda sneha
 *
 */
/*
 * Java method to display the middle character of a string. a) If the length of
 * the string is odd there will be one middle characters. b) If the length of
 * the string is even there will be two middle character. Input a string: 367
 * Expected Output: The middle character in the string: 6
 * 
 */
public class FindMiddleChar {
	public String toFindCharacter(String input) {
		int length = input.length();
		int middleindex = length / 2;
		String output = "";
		if (length % 2 != 0)
			output = output + input.charAt(middleindex);
		else
			output = output + input.substring(middleindex - 1, middleindex + 1);
		return output;
	}

}
