package com.pojo;

/***
 * 
 * @author gunda sneha
 *
 */
/*
 * Java method to count all vowels in a string. Input the string: Hcl
 * Technologies Expected Output: Number of Vowels in the string: 5
 * 
 * 
 */

public class CountVowels {
	public int Count_Vowels(String string) {
		int count = 0;
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == 'a' || string.charAt(i) == 'e' || string.charAt(i) == 'i' || string.charAt(i) == 'o'
					|| string.charAt(i) == 'u')

			{
				count++;
			}
		}
		return count;
	}

}
