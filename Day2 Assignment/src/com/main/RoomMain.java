package com.main;

import com.pojo.Room;

public class RoomMain {

	public static void main(String[] args) {
		Room room=new Room();
		room.setRoomNo(22);
		room.setRoomType("Delux");
		room.setRoomArea(444.5f);
		room.setAcMachine(true);
		room.display();

	}

}
