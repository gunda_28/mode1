package com.main;

import java.util.Scanner;

import com.pojo.Addition;

public class AdditionMain {

	public static void main(String[] args) {
		Addition addition = new Addition();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the first number");
		int num1 = scanner.nextInt();
		System.out.println("Enter the second number");
		int num2 = scanner.nextInt();
		System.out.println("Enter the third number");
		int num3 = scanner.nextInt();
		System.out.println("Enter the fourth number");
		int num4 = scanner.nextInt();
		System.out.println("Enter the fifth number");
		int num5 = scanner.nextInt();
		System.out.println("Enter the sixth number");
		int num6 = scanner.nextInt();
		System.out.println(num1 + "+" + num2 + "=" + addition.add(num1, num2));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "=" + addition.add(num1, num2, num3));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "+" + num4 + "=" + addition.add(num1, num2, num3, num4));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "+" + num4 + "+" + num5 + "="
				+ addition.add(num1, num2, num3, num4, num5));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "+" + num4 + "+" + num5 + "+" + num6 + "="
				+ addition.add(num1, num2, num3, num4, num5, num6));

	}

}
