package com.main;

import com.pojo.FindMiddleChar;

public class MiddleCharacterMain {

	public static void main(String[] args) {
	 FindMiddleChar findMiddleChar=new FindMiddleChar();
	 System.out.println("for odd length" + findMiddleChar.toFindCharacter("367"));
	 System.out.println("for even length " + findMiddleChar.toFindCharacter("snehag"));
	}

}
