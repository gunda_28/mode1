package com.main;

import java.util.Scanner;

import com.pojo.AdvancedArithemetic;
import com.pojo.MyCalculator;

public class AdvancedArithemeticMain {

	public static void main(String[] args) {
		AdvancedArithemetic mycalculator = new MyCalculator();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number : ");
		int number = scanner.nextInt();
		System.out.println("The Divisor Sum of the Number "+number+" is : "+ mycalculator.divisorSum(number));
		
		scanner.close();
		

	}

}
