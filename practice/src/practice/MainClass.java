package practice;

public class MainClass {

	public static void main(String[] args) {
		Employee employee1 = new Employee();
		System.out.println(employee1);
		employee1.EmpName = "Sneha";
		employee1.Salary = 2800f;
		Employee employee2 = new Employee();
		System.out.println(employee2);
		employee2.EmpName = "Sara";
		employee2.Salary = 2900f;
		if (employee1.Salary > employee2.Salary) {
			System.out.println(employee1.EmpName + " has highest salary " + employee1.Salary);

		} else {
			System.out.println(employee2.EmpName + " has highest salary " + employee2.Salary);

		}
		EmployeeService employeeService = new EmployeeService();
		System.out.println(employeeService);
		employeeService.sayHello();
		employeeService.displayName(" HCL ");
		employeeService.add(20, 40);
		String Var = employeeService.getName();
		System.out.println(Var);
		String Var1 = employeeService.getName(" newyear ");
		System.out.println(Var1);
		System.out.println(employeeService.getEmployeeById(51931005));
		Employee ans = employeeService.getEmployeeDetail(112233);
		System.out.println(ans.empNo);
		System.out.println(ans.firstName);
		System.out.println(ans.secondName);
		System.out.println(ans.Salary);
		float money = employeeService.updateSalary(ans);
		System.out.println("Salary hike" + money);

		int[] myArray = { 1, 2, 3, 4, 5 };
		// employeeService=null;
		// employee1=null;//de referencing
		// employee2=null;
		int res = employeeService.addOfArray(myArray);
		System.out.println(res);

	}

}
